<?php

namespace Drupal\realty;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a realty entity type.
 */
interface RealtyInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
