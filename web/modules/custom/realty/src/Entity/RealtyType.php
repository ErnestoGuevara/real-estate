<?php

namespace Drupal\realty\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Realty type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "realty_type",
 *   label = @Translation("Realty type"),
 *   label_collection = @Translation("Realty types"),
 *   label_singular = @Translation("realty type"),
 *   label_plural = @Translation("realties types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count realties type",
 *     plural = "@count realties types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\realty\Form\RealtyTypeForm",
 *       "edit" = "Drupal\realty\Form\RealtyTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\realty\RealtyTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer realty types",
 *   bundle_of = "realty",
 *   config_prefix = "realty_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/realty_types/add",
 *     "edit-form" = "/admin/structure/realty_types/manage/{realty_type}",
 *     "delete-form" = "/admin/structure/realty_types/manage/{realty_type}/delete",
 *     "collection" = "/admin/structure/realty_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class RealtyType extends ConfigEntityBundleBase {

  /**
   * The machine name of this realty type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the realty type.
   *
   * @var string
   */
  protected $label;

}
